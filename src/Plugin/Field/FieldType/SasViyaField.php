<?php

namespace Drupal\sas_viya\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\file\Entity\File;
use Drupal\file\Plugin\Field\FieldType\FileItem;


/**
 * @FieldType(
 *   id = "sas_viya",
 *   module = "sas_viya",
 *   label = @Translation("SAS Viya chart"),
 *   category = @Translation("Reference"),
 *   description = @Translation("This field type stores SAS Viya chart reference information."),
 *   default_widget = "sas_viya_widget",
 *   default_formatter = "sas_viya_formatter",
 * )
 */

class SasViyaField extends FieldItemBase {

  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {

    $properties = [];

    $properties['object_name'] = DataDefinition::create('string')
      ->setLabel(t('Object name'))
      ->setDescription(t('The name of the chart object.'));

    $properties['report_uri'] = DataDefinition::create('string')
      ->setLabel(t('Report URI'))
      ->setDescription(t('The path to the report.'));

    return $properties;
  }

  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $columns = array(
      'object_name' => array(
        'type' => 'varchar',
        'length' => 255,
      ),
      'report_uri' => array(
        'type' => 'varchar',
        'length' => 255,
      ),
    );

    $schema = array(
      'columns' => $columns,
      'indexes' => [],
    );

    return $schema;
  }

  
  public function isEmpty() {
    return empty($this->values['object_name']) && empty($this->values['report_uri']);
  }
}
