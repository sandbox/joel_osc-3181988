<?php

namespace Drupal\sas_viya\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'NameFieldTypeDefaultWidget' widget.
 *
 * @FieldWidget(
 *   id = "sas_viya_widget",
 *   label = @Translation("SAS Viya chart reference"),
 *   description = @Translation("Use to reference SAS Viya charts"),
 *   field_types = {
 *     "sas_viya",
 *   }
 * )
 */

class SasViyaWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */

  public function formElement( FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    $element += [
      '#type' => 'fieldset',
    ];

    $element['object_name'] = [
      '#type' => 'textfield',
      '#title' => t('Object name'),
      '#description' => t('The name of the chart object within the report. <em>Ex. ve12</em>'),
      '#default_value' => isset($items[$delta]->object_name) ? $items[$delta]->object_name : NULL,
      '#size' => 10,
    ];

    $element['report_uri'] = [
      '#type' => 'textfield',
      '#title' => t('Report URI'),
      '#description' => t('The path to the report. <em>Ex. /reports/my-reports/ce73d96e-5neb-4gd9-bp3a-3oe0d268d4b5</em>'),
      '#default_value' => isset($items[$delta]->report_uri) ? $items[$delta]->report_uri : NULL,
      '#size' => 155,
    ];

    return $element;
  }
}
