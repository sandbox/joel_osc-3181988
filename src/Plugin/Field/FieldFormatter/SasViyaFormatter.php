<?php

namespace Drupal\sas_viya\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\sas_viya\Util\ConfigUtil;

/**
 * Plugin implementation of the 'sas_viya_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "sas_viya_formatter",
 *   label = @Translation("SAS Viya chart"),
 *   field_types = {
 *     "sas_viya"
 *   }
 * )
 */

class SasViyaFormatter extends FormatterBase {

  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];
    foreach ($items as $delta => $item) {
      $element[$delta] = [
        '#type' => 'inline_template',
        '#template' => '<sas-report-object objectName="{{ object_name }}" authenticationType="guest" url="{{ server }}" reportUri="{{ report_uri }}"></sas-report-object>',
        '#context' => [
          'report_uri' => $item->report_uri,
          'object_name' => $item->object_name,
          'server' => ConfigUtil::getServer(),
        ],
      ];
    }

    return $element;
  }
}
