This project provides a field (type, widget, and formatter) and a global configuration page that enables Drupal to render (proprietary) SAS Viya charts.

It's quite simple to use:

1. just enable the module
2. go to the SAS Viya settings page and configure the server information
3. add a SAS Viya reference field to a content type
4. create a node with the SAS Viya chart object name and report uri and save

Now you should see your chart rendered on node view.
