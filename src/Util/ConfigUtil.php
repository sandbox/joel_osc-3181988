<?php

namespace Drupal\sas_viya\Util;

class ConfigUtil {

  private static function GetConfig() {
    return \Drupal::config('sas_viya.settings');
  }


  /**
   * @return string
   */
  public static function GetServer() {
    $config = self::GetConfig();
    return ($config->get('sas_viya_mode') == 'devel') ? $config->get('dev_server') : $config->get('prod_server'); 
  }
}
