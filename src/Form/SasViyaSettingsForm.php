<?php

namespace Drupal\sas_viya\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure settings for SAS Viya.
 */
class SasViyaSettingsForm extends ConfigFormBase {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sas_viya_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'sas_viya.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('sas_viya.settings');

    $form['sas_viya_mode'] = [
      '#type' => 'radios',
      '#title' => $this->t('SAS Viya mode'),
      '#description' => $this->t('In production mode the charts will render using the production server and in development mode the development server.'),
      '#options' => ['devel' => $this->t('Development'), 'prod' => $this->t('Production')],
      '#required' => TRUE,
      '#default_value' => empty($config->get('sas_viya_mode')) ? 'devel' : $config->get('sas_viya_mode'),
    ];

    $form['dev_server'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Development server url'),
      '#description' => $this->t('The url to the development SAS Viya server. <em>Ex. https://devviya.mydomain.com</em>'),
      '#default_value' => empty($config->get('dev_server')) ? '' : $config->get('dev_server'),
    ];

    $form['prod_server'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Production server url'),
      '#description' => $this->t('The url to the production SAS Viya server. <em>Ex. https://prodviya.mydomain.com</em>'),
      '#default_value' => empty($config->get('prod_server')) ? '' : $config->get('prod_server'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    //$dev_server  = $form_state->getValue('dev_server');
    //$prod_server = $form_state->getValue('prod_server');
    //$sas_viya_mode = $form_state->getValue('sas_viya_mode');
    $this->config('sas_viya.settings')
      ->set('sas_viya_mode', $form_state->getValue('sas_viya_mode'))
      ->set('dev_server', $form_state->getValue('dev_server'))
      ->set('prod_server', $form_state->getValue('prod_server'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
